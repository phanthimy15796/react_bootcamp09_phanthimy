import React, { Component } from "react";
import footer1 from "./img/promotion_1.png";

class Imgfooter extends Component {
  render() {
    return (
      <div className="bg-light">
        <div className="m-5">
          <img src={footer1} alt style={{ maxWidth: "100%" }} />
        </div>
      </div>
    );
  }
}

export default Imgfooter;
