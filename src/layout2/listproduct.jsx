import React, { Component } from 'react';
import Itemproduct from './itemproduct';
class Listproduct extends Component {
    render() {
        return (
            <div className="container-fluid">
                <h2 className="text-center mb-4 text-white">DMSP</h2>
                <div className="row">
                    <div className="col-3">
                        <Itemproduct/>
                    </div>
                    <div className="col-3">
                        <Itemproduct/>
                    </div>
                    <div className="col-3">
                        <Itemproduct/>
                    </div>
                    <div className="col-3">
                        <Itemproduct/>
                    </div>
                </div> 
                
            </div>
        );
    }
}

export default Listproduct;