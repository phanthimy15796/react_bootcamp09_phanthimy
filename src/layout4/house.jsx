import React, { Component } from "react";
import Header from "./header";
import Slider from "./slider";
import Aboutus from "./aboutus";
import Contact from "./contact";
import Cardtitle from "./cardtitle";
import Footer from "./footer";
class House extends Component {
  render() {
    return (
      <div>
        <Header />
        <Slider />
        <section>
          <div className="container">
            <div className="row">
              <div className="col-md-8">
                <Aboutus />
              </div>
              <div className="col-md-4">
                <Contact />
              </div>
            </div>
          </div>
        </section>
        <Cardtitle></Cardtitle>
        <Footer/>
      </div>
    );
  }
}

export default House;
