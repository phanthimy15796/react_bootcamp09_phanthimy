import React, { Component } from "react";
import Itemcard from "./itemcard";
class Cardtitle extends Component {
  render() {
    return (
      <section>
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <Itemcard />
            </div>
            <div className="col-md-4">
              <Itemcard />
            </div>
            <div className="col-md-4">
              <Itemcard />
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default Cardtitle;
